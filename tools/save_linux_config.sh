#! /bin/bash

set -e

cd ${HOME}

list="tools/ .zshrc .zsh/ .uim .cache/userconfig .config/nvim"
save_dir="my_linux_config"

if [ ! -e $save_dir ]; then
    git clone https://sa10dai@bitbucket.org/sa10dai/${save_dir}.git
fi

set +e
cp -r $list ${save_dir}/
set -e

cd ${save_dir}
git add .
git commit
git push origin master

