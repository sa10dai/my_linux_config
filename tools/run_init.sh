# aptレポジトリを日本に変更
sudo sed -i.bak -e "s%http://[^ ]\+%http://ftp.jaist.ac.jp/pub/Linux/ubuntu/%g" /etc/apt/sources.list

sudo apt-get update
sudo apt-get upgrade

sudo apt-get install language-pack-ja

# python
sudo apt-get install python python3 python-pip python3-pip

# zsh
sudo apt-get install zsh
chsh -s $(wich zsh)
zsh

# zeovim
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:neovim-ppa/stable
sudo apt-get update
sudo apt-get install neovim
pip install neovim
pip3 install neovim

# Xserver
sudo apt install fonts-ipafont
sudo apt-get install gnuplot
sudo apt-get install uim uim-xim uim-anthy
sudo apt-get install gnome-terminal
